# encoding = utf-8

# import base64
# from Crypto.Cipher import AES
#
#
# # 密钥（key）, 密斯偏移量（iv） CBC模式加密
#
# def AES_Encrypt(key, data):
#     vi = '0102030405060708'
#     pad = lambda s: s + (16 - len(s) % 16) * chr(16 - len(s) % 16)
#     data = pad(data)
#     # 字符串补位
#     cipher = AES.new(key.encode('utf8'), AES.MODE_CBC, vi.encode('utf8'))
#     encryptedbytes = cipher.encrypt(data.encode('utf8'))
#     # 加密后得到的是bytes类型的数据
#     encodestrs = base64.b64encode(encryptedbytes)
#     # 使用Base64进行编码,返回byte字符串
#     enctext = encodestrs.decode('utf8')
#     # 对byte字符串按utf-8进行解码
#     return enctext
#
#
# def AES_Decrypt(key, data):
#     vi = '0102030405060708'
#     data = data.encode('utf8')
#     encodebytes = base64.decodebytes(data)
#     # 将加密数据转换位bytes类型数据
#     cipher = AES.new(key.encode('utf8'), AES.MODE_CBC, vi.encode('utf8'))
#     text_decrypted = cipher.decrypt(encodebytes)
#     unpad = lambda s: s[0:-s[-1]]
#     text_decrypted = unpad(text_decrypted)
#     # 去补位
#     text_decrypted = text_decrypted.decode('utf8')
#     return text_decrypted
#
#
# key = '0CoJUm6Qyw8W8jud'
# data = 'sdadsdsdsfd'
# AES_Encrypt(key, data)
# enctext = AES_Encrypt(key, data)
# print(enctext)
# text_decrypted = AES_Decrypt(key, enctext)
# print(text_decrypted)
import asyncio
import os
import base64
import time
from shutil import rmtree
from progressbar import *
from Crypto.Cipher import AES


# 密钥（key）, 密斯偏移量（iv） CBC模式加密

# def AES_Encrypt(key, data):
#     vi = '0x00000000000000000000000000000000'
#     pad = lambda s: s + (16 - len(s) % 16) * chr(16 - len(s) % 16)
#     data = pad(data)
#     # 字符串补位
#     cipher = AES.new(key.encode('utf8'), AES.MODE_CBC, vi.encode('utf8'))
#     encryptedbytes = cipher.encrypt(data.encode('utf8'))
#     # 加密后得到的是bytes类型的数据
#     encodestrs = base64.b64encode(encryptedbytes)
#     # 使用Base64进行编码,返回byte字符串
#     enctext = encodestrs.decode('utf8')
#     # 对byte字符串按utf-8进行解码
#     return enctext


# def AES_Decrypt(key, data):
#     vi = '0102030405060708'
#     data = data.encode('utf8')
#     encodebytes = base64.decodebytes(data)
#     # 将加密数据转换位bytes类型数据
#     cipher = AES.new(key.encode('utf8'), AES.MODE_CBC, vi.encode('utf8'))
#     text_decrypted = cipher.decrypt(encodebytes)
#     unpad = lambda s: s[0:-s[-1]]
#     text_decrypted = unpad(text_decrypted)
#     # 去补位
#     text_decrypted = text_decrypted.decode('utf8')
#     return text_decrypted



#  解码TS
from icecream import ic
# base_path =r'G:\爬虫第四章\scrapy数据提取'
def concat_ts(base_path):
    iv = '0000000000000000'
    with open(fr'{base_path}\key.key','rb') as f:
        key = f.read()
        ic(key)
        cryptor = AES.new(key, AES.MODE_CBC, iv.encode('utf8'))
        ts = os.listdir(fr'{base_path}\ts')
        ic(len(ts))
        for i in range(len(ts)):
            with open(fr'{base_path}\mp4\{i}.mp4', 'wb') as jj:
                with open(fr'{base_path}\ts\{i}.ts','rb')as data1:
                    data = cryptor.decrypt(data1.read())
                    jj.write(data)
            ic(fr'{i}.ts>>>>>>>>done')

#合并MP4
def concat_mp4(base_path):
    ts = os.listdir(fr'{base_path}\mp4')
    name = base_path.split('\\')[2]
    (len(ts))
    with open(fr'{base_path}\{name}.mp4', 'wb+') as jj:
        for i in range(len(ts)):
            with open(fr'{base_path}\mp4\{i}.mp4','rb')as data1:
                data = data1.read()
                jj.write(data)
            # ic(fr'{i}.mp4>>>>>>>>done')
            ic(i)

# concat_ts()
# concat_mp4()
    rmtree(fr'{base_path}\ts')
    rmtree(fr'{base_path}\mp4')