import asyncio
import time
import aiohttp


def name():
    print('菲尔，你好')
    time.sleep(3)
    print('你好可爱')

if __name__ == '__main__':
    s = time.time()
    name()
    e = time.time()
    print(e - s)

#  异步携程
async def names():
    print('菲尔你好')
    await asyncio.sleep(3)
    print('你好可爱呀')

async def names1():
    print('菲尔你好1')
    await asyncio.sleep(3)
    print('你好可爱呀1')

if __name__ == '__main__':
    s = time.time()
    tasks = [names(),
             names1()
             ]
    asyncio.run(asyncio.wait(tasks))
    e = time.time()
    print(e-s)

